```shell
# 可以不带后面内容，只是为了把相关类dump下来
java -Xshare:off -XX:+UseAppCDS -XX:DumpLoadedClassList=hello.lst HelloWorld

java -Xshare:dump -XX:+UseAppCDS -XX:SharedClassListFile=hello.lst -XX:SharedArchiveFile=hello.jsa HelloWorld

java -Xshare:on -XX:+UseAppCDS -XX:SharedArchiveFile=hello.jsa HelloWorld
```



```shell
# load
./bin/ycsb load mongodb -jvm-args "-Xshare:off -XX:+UseAppCDS -XX:DumpLoadedClassList=load.lst" -s -P workloads/workloada

./bin/ycsb load mongodb -jvm-args "-Xshare:dump -XX:+UseAppCDS -XX:SharedClassListFile=load.lst -XX:SharedArchiveFile=load.jsa" -s -P workloads/workloada

./bin/ycsb load mongodb -jvm-args "-Xshare:on -XX:+UseAppCDS -XX:SharedArchiveFile=load.jsa" -s -P workloads/workloada


# run
./bin/ycsb run mongodb -jvm-args "-Xshare:off -XX:+UseAppCDS -XX:DumpLoadedClassList=load.lst" -s -P workloads/workloada

./bin/ycsb run mongodb -jvm-args "-Xshare:dump -XX:+UseAppCDS -XX:SharedClassListFile=load.lst -XX:SharedArchiveFile=load.jsa" -s -P workloads/workloada

./bin/ycsb run mongodb -jvm-args "-Xshare:on -XX:+UseAppCDS -XX:SharedArchiveFile=load.jsa" -s -P workloads/workloada
```



```python
# runWithAppCDS
import time
import os

if __name__ == '__main__':
    start = time.time()
    for i in range(1000):
        os.system("java -Xshare:on -XX:+UseAppCDS -XX:SharedArchiveFile=hello.jsa -cp hello.jar HelloWorld > /dev/null 2>&1")
    end = time.time()
    print((end-start))
    
# runWithoutAppCDS
import time
import os

if __name__ == '__main__':
    start = time.time()
    for i in range(1000):
        os.system("java -cp hello.jar HelloWorld > /dev/null 2>&1")
    end = time.time()
    print((end-start))
    
```



```shell
# 测试通过
java -Xshare:off -XX:+UseAppCDS -XX:DumpLoadedClassList=hello.lst TestRun

java -Xshare:dump -XX:+UseAppCDS -XX:SharedClassListFile=hello.lst -XX:SharedArchiveFile=hello.jsa TestRun

java -Xshare:on -XX:+UseAppCDS -XX:SharedArchiveFile=hello.jsa LongRun

# 一个线程运行LongRun 然后一个python调用300次TestRun
java -Xshare:off -XX:+UseAppCDS -XX:DumpLoadedClassList=hello.lst LongRun

java -Xshare:dump -XX:+UseAppCDS -XX:SharedClassListFile=hello.lst -XX:SharedArchiveFile=hello.jsa LongRun

java -Xshare:on -XX:+UseAppCDS -XX:SharedArchiveFile=hello.jsa LongRun

# part1
java -Xshare:on -XX:+UseAppCDS -XX:SharedArchiveFile=hello.jsa LongRun

# part2 runWithAppCDS.py
import time
import os

if __name__ == '__main__':
    start = time.time()
    for i in range(1000):
        os.system("java -Xshare:on -XX:+UseAppCDS -XX:SharedArchiveFile=hello.jsa TestRun > /dev/null 2>&1")
    end = time.time()
    print((end-start))
    
# part3 runWithoutAppCDS.py
import time
import os

if __name__ == '__main__':
    start = time.time()
    for i in range(1000):
        os.system("java TestRun > /dev/null 2>&1")
    end = time.time()
    print((end-start))
    
# runWithoutAppCDS.py 耗时：79.4280309677124
# runWithAppCDS.py 耗时：45.770809173583984
# runWithAppCDS.py + LongRun 耗时：44.72335958480835
```

