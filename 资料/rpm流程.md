```shell
yum install rpm-build

##### one of two -part1
yum install rpmdevtools
rpmdev-setuptree
##### one of two -part2
mkdir -p ~/rpmbuild/{BUILD,RPMS,SOURCES,SPECS,SRPMS}

cd ~/rpmbuild
cp source_path/xxx.spec ~/rpmbuild/SPECS
cp source_path/* ~/rpmbuild/SOURCES
rm -f xxx.spec README*

rpmbuild -ba xxx.spec

```

