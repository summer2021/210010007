%global _hardened_build 1

%global hadoop_version %{version}
%global hdfs_services hadoop-zkfc.service hadoop-datanode.service hadoop-secondarynamenode.service hadoop-namenode.service hadoop-journalnode.service
%global mapreduce_services hadoop-historyserver.service
%global yarn_services hadoop-proxyserver.service hadoop-resourcemanager.service hadoop-nodemanager.service hadoop-timelineserver.service

# Filter out undesired provides and requires
%global __requires_exclude_from ^%{_libdir}/%{real_name}/libhadoop.so$
%global __provides_exclude_from ^%{_libdir}/%{real_name}/.*$
%define real_name hadoop
%define _binaries_in_noarch_packages_terminate_build 0s

%global debug_package %{nil}


Name:           hadoop-3.1
Version:        3.1.4
Release:        1
Summary:        This is a java rpmbuild tests

License:        Apache-2.0 and BSD and Zlib and BSL-1.0 and MPL-2.0 and EPL-1.0 and MIT

BuildRequires:  java-1.8.0-openjdk-devel
Requires:       java-1.8.0-openjdk

Source0: https://www.apache.org/dist/%{real_name}/core/%{real_name}-%{version}/%{real_name}-%{version}-src.tar.gz
Source1: %{real_name}-layout.sh
Source2: %{real_name}-hdfs.service.template
Source3: %{real_name}-mapreduce.service.template
Source4: %{real_name}-yarn.service.template
Source5: context.xml
Source6: %{real_name}.logrotate
Source7: %{real_name}-httpfs.sysconfig
Source8: hdfs-create-dirs
Source9: %{real_name}-tomcat-users.xml
Source10: %{real_name}-core-site.xml
Source11: %{real_name}-hdfs-site.xml
Source12: %{real_name}-mapred-site.xml
Source13: %{real_name}-yarn-site.xml

Patch1: 0001-sys_errlist-undeclared.patch

BuildRoot: %{_tmppath}/%{real_name}-%{version}-%{release}-root
BuildRequires: java-1.8.0-openjdk-devel maven hostname maven-local tomcat cmake snappy openssl-devel 
BuildRequires: cyrus-sasl-devel chrpath systemd protobuf2-compiler protobuf2-devel protobuf2-java protobuf2
Buildrequires: leveldbjni leveldb-java hawtjni-runtime
Requires: java-1.8.0-openjdk protobuf2-java apache-zookeeper

%description 
Apache Hadoop is a framework that allows for the distributed processing of
large data sets across clusters of computers using simple programming models.
It is designed to scale up from single servers to thousands of machines, each
offering local computation and storage.


%prep
%autosetup -p1 -n %{real_name}-%{version}-src
mvn install:install-file -DgroupId=com.google.protobuf -DartifactId=protoc -Dversion=2.5.0 -Dclassifier=linux-aarch_64 -Dfile=/usr/bin/protoc -e -Dpackaging=exe
mvn install:install-file -DgroupId=org.fusesource.leveldbjni -DartifactId=leveldbjni-all -Dversion=1.8 -Dpackaging=jar -Dfile=/usr/lib/java/leveldbjni-all.jar
mvn install:install-file -DgroupId=org.fusesource.leveldbjni -DartifactId=leveldbjni -Dversion=1.8 -Dpackaging=jar -Dfile=/usr/lib/java/leveldbjni/leveldbjni.jar
mvn install:install-file -DgroupId=org.iq80.leveldb -DartifactId=leveldb-api -Dversion=0.7 -Dpackaging=jar -Dfile=/usr/share/java/leveldb-java/leveldb-api.jar
mvn install:install-file -DgroupId=org.iq80.leveldb -DartifactId=leveldb-benchmark -Dversion=0.7 -Dpackaging=jar -Dfile=/usr/share/java/leveldb-java/leveldb-benchmark.jar
mvn install:install-file -DgroupId=org.iq80.leveldb -DartifactId=leveldb -Dversion=0.7 -Dpackaging=jar -Dfile=/usr/share/java/leveldb-java/leveldb.jar
mvn install:install-file -DgroupId=orn.fusesource.hawtjni -DartifactId=hawtjni-runtime -Dversion=1.16 -Dpackaging=jar -Dfile=/usr/lib/java/hawtjni/hawtjni-runtime.jar

%pom_add_dep org.iq80.leveldb:leveldb-api:0.7 hadoop-hdfs-project/hadoop-hdfs
%pom_add_dep org.iq80.leveldb:leveldb-api:0.7 hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-web-proxy
%pom_add_dep org.iq80.leveldb:leveldb-api:0.7 hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-applicationhistoryservice
%pom_add_dep org.iq80.leveldb:leveldb-api:0.7 hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-common
%pom_add_dep org.fusesource.leveldbjni:leveldbjni:1.8 hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server
%pom_add_dep org.fusesource.hawtjni:hawtjni-runtime:1.16 hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/hadoop-yarn-server-applicationhistoryservice

%pom_disable_module hadoop-minikdc hadoop-common-project
%pom_disable_module hadoop-pipes hadoop-tools
%pom_disable_module hadoop-azure hadoop-tools
%pom_disable_module hadoop-yarn-server-timelineservice-hbase-tests hadoop-yarn-project/hadoop-yarn/hadoop-yarn-server/pom.xml


# War files we don't want
%mvn_package :%{real_name}-auth-examples __noinstall
%mvn_package :%{real_name}-hdfs-httpfs __noinstall

# Parts we don't want to distribute
%mvn_package :%{real_name}-assemblies __noinstall

# Workaround for bz1012059
%mvn_package :%{real_name}-project-dist __noinstall

# Create separate file lists for packaging
%mvn_package :::tests: %{real_name}-tests
%mvn_package :%{real_name}-*-tests::{}: %{real_name}-tests
%mvn_package :%{real_name}-client*::{}: %{real_name}-client
%mvn_package :%{real_name}-hdfs*::{}: %{real_name}-hdfs
%mvn_package :%{real_name}-mapreduce-examples*::{}: %{real_name}-mapreduce-examples
%mvn_package :%{real_name}-mapreduce*::{}: %{real_name}-mapreduce
%mvn_package :%{real_name}-archives::{}: %{real_name}-mapreduce
%mvn_package :%{real_name}-datajoin::{}: %{real_name}-mapreduce
%mvn_package :%{real_name}-distcp::{}: %{real_name}-mapreduce
%mvn_package :%{real_name}-extras::{}: %{real_name}-mapreduce
%mvn_package :%{real_name}-gridmix::{}: %{real_name}-mapreduce
%mvn_package :%{real_name}-openstack::{}: %{real_name}-mapreduce
%mvn_package :%{real_name}-rumen::{}: %{real_name}-mapreduce
%mvn_package :%{real_name}-sls::{}: %{real_name}-mapreduce
%mvn_package :%{real_name}-streaming::{}: %{real_name}-mapreduce
%mvn_package :%{real_name}-tools*::{}: %{real_name}-mapreduce
%mvn_package :%{real_name}-maven-plugins::{}: %{real_name}-maven-plugin
%mvn_package :%{real_name}-minicluster::{}: %{real_name}-tests
%mvn_package :%{real_name}-yarn*::{}: %{real_name}-yarn

# Jar files that need to be overridden due to installation location
%mvn_file :%{real_name}-common::tests: %{real_name}/%{real_name}-common


%build
mvn -Dsnappy.lib=/usr/lib64 -Dbundle.snappy -Dcontainer-executor.conf.dir=%{_sysconfdir}/%{real_name} -Pdist,native -DskipTests -DskipIT -Dmaven.javadoc.skip=true package


%install
# Copy all jar files except those generated by the build
# $1 the src directory
# $2 the dest directory
copy_dep_jars()
{
  find $1 ! -name "hadoop-*.jar" -name "*.jar" | xargs install -m 0644 -t $2
  rm -f $2/tools-*.jar
}

# Create symlinks for jars from the build
# $1 the location to create the symlink
link_hadoop_jars()
{
  for f in `ls hadoop-* | grep -v tests | grep -v examples`
  do
    n=`echo $f | sed "s/-%{version}//"`
    if [ -L $1/$n ]
    then
      continue
    elif [ -e $1/$f ]
    then
      rm -f $1/$f $1/$n
    fi
    p=`find %{buildroot}%{_jnidir} %{buildroot}%{_javadir}/%{real_name} -name $n | sed "s#%{buildroot}##"`
    %{__ln_s} $p $1/$n
  done
}

%mvn_install

install -d -m 0755 %{buildroot}%{_libdir}/%{real_name}
install -d -m 0755 %{buildroot}%{_includedir}/%{real_name}
install -d -m 0755 %{buildroot}%{_jnidir}/%{real_name}

install -d -m 0755 %{buildroot}%{_datadir}/%{real_name}/client/lib
install -d -m 0755 %{buildroot}%{_datadir}/%{real_name}/common/lib
install -d -m 0755 %{buildroot}%{_datadir}/%{real_name}/hdfs/lib
install -d -m 0755 %{buildroot}%{_datadir}/%{real_name}/hdfs/webapps
install -d -m 0755 %{buildroot}%{_datadir}/%{real_name}/httpfs/tomcat/webapps
install -d -m 0755 %{buildroot}%{_datadir}/%{real_name}/mapreduce/lib
install -d -m 0755 %{buildroot}%{_datadir}/%{real_name}/yarn/lib
install -d -m 0755 %{buildroot}%{_sysconfdir}/%{real_name}/tomcat/Catalina/localhost
install -d -m 0755 %{buildroot}%{_sysconfdir}/logrotate.d
install -d -m 0755 %{buildroot}%{_sysconfdir}/sysconfig
install -d -m 0755 %{buildroot}%{_tmpfilesdir}
install -d -m 0755 %{buildroot}%{_sharedstatedir}/%{real_name}-hdfs
install -d -m 0755 %{buildroot}%{_sharedstatedir}/tomcats/httpfs
install -d -m 0755 %{buildroot}%{_var}/cache/%{real_name}-yarn
install -d -m 0755 %{buildroot}%{_var}/cache/%{real_name}-httpfs/temp
install -d -m 0755 %{buildroot}%{_var}/cache/%{real_name}-httpfs/work
install -d -m 0755 %{buildroot}%{_var}/cache/%{real_name}-mapreduce
install -d -m 0755 %{buildroot}%{_var}/log/%{real_name}-yarn
install -d -m 0755 %{buildroot}%{_var}/log/%{real_name}-hdfs
install -d -m 0755 %{buildroot}%{_var}/log/%{real_name}-httpfs
install -d -m 0755 %{buildroot}%{_var}/log/%{real_name}-mapreduce
install -d -m 0755 %{buildroot}%{_var}/run/%{real_name}-yarn
install -d -m 0755 %{buildroot}%{_var}/run/%{real_name}-hdfs
install -d -m 0755 %{buildroot}%{_var}/run/%{real_name}-mapreduce

basedir='%{real_name}-common-project/%{real_name}-common/target/%{real_name}-common-%{hadoop_version}'
hdfsdir='%{real_name}-hdfs-project/%{real_name}-hdfs/target/%{real_name}-hdfs-%{hadoop_version}'
httpfsdir='%{real_name}-hdfs-project/%{real_name}-hdfs-httpfs/target/%{real_name}-hdfs-httpfs-%{hadoop_version}'
mapreddir='%{real_name}-mapreduce-project/target/%{real_name}-mapreduce-%{hadoop_version}'
yarndir='%{real_name}-yarn-project/target/%{real_name}-yarn-project-%{hadoop_version}'

# copy jar package
install -d -m 0755 %{buildroot}%{_datadir}/java/%{real_name}
install -d -m 0755 %{buildroot}%{_datadir}/maven-poms/%{real_name}

install -m 0755 %{real_name}-client-modules/%{real_name}-client/target/hadoop-client-%{version}.jar %{buildroot}%{_datadir}/java/%{real_name}/hadoop-client.jar
echo %{_datadir}/java/%{real_name}/hadoop-client.jar >> .mfiles-hadoop-client 
install -m 0755 %{real_name}-client-modules/%{real_name}-client/pom.xml %{buildroot}%{_datadir}/maven-poms/%{real_name}/hadoop-client.pom 
echo %{_datadir}/maven-poms/%{real_name}/hadoop-client.pom >> .mfiles-hadoop-client
install -m 0755 %{real_name}-client-modules/%{real_name}-client-api/target/hadoop-client-api-%{version}.jar %{buildroot}%{_datadir}/java/%{real_name}/hadoop-client-api.jar
echo %{_datadir}/java/%{real_name}/hadoop-client-api.jar >> .mfiles-hadoop-client
install -m 0755 %{real_name}-client-modules/%{real_name}-client-api/pom.xml %{buildroot}%{_datadir}/maven-poms/%{real_name}/hadoop-client-api.pom
echo %{_datadir}/maven-poms/%{real_name}/hadoop-client-api.pom >> .mfiles-hadoop-client
install -m 0755 %{real_name}-client-modules/%{real_name}-client-minicluster/target/hadoop-client-minicluster-%{version}.jar %{buildroot}%{_datadir}/java/%{real_name}/hadoop-client-minicluster.jar
echo %{_datadir}/java/%{real_name}/hadoop-client-minicluster.jar >> .mfiles-hadoop-client
install -m 0755 %{real_name}-client-modules/%{real_name}-client-minicluster/pom.xml %{buildroot}%{_datadir}/maven-poms/%{real_name}/hadoop-client-minicluster.pom
echo %{_datadir}/maven-poms/%{real_name}/hadoop-client-minicluster.pom >> .mfiles-hadoop-client
install -m 0755 %{real_name}-client-modules/%{real_name}-client-runtime/target/hadoop-client-runtime-%{version}.jar %{buildroot}%{_datadir}/java/%{real_name}/hadoop-client-runtime.jar
echo %{_datadir}/java/%{real_name}/hadoop-client-runtime.jar >> .mfiles-hadoop-client
install -m 0755 %{real_name}-client-modules/%{real_name}-client-runtime/pom.xml %{buildroot}%{_datadir}/maven-poms/%{real_name}/hadoop-client-runtime.pom
echo %{_datadir}/maven-poms/%{real_name}/hadoop-client-runtime.pom >> .mfiles-hadoop-client


%files 
%{_datadir}/%{real_name}/client