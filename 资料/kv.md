| k                                            | v                                                            |
| -------------------------------------------- | ------------------------------------------------------------ |
| _hardened_build                              | 1                                                            |
| %{version}                                   | **3.1**                                                      |
| hadoop_version                               | %{version}                                                   |
| hdfs_services                                | hadoop-zkfc.service hadoop-datanode.service hadoop-secondarynamenode.service hadoop-namenode.service hadoop-journalnode.service |
| mapreduce_services                           | hadoop-historyserver.service                                 |
| yarn_services                                | hadoop-proxyserver.service hadoop-resourcemanager.service hadoop-nodemanager.service hadoop-timelineserver.service |
| %{_libdir}                                   | /usr/lib64                                                   |
| real_name                                    | hadoop                                                       |
| _binaries_in_noarch_packages_terminate_build | 0                                                            |
| %{_tmppath}                                  | /var/tmp                                                     |
| %{?_isa}                                     | (aarch-64)                                                   |
| %pom_add_dep                                 | /usr/bin/python3 /usr/share/java-utils/pom_editor.py pom_add_dep |
| %pom_disable_module                          | /usr/bin/python3 /usr/share/java-utils/pom_editor.py pom_disable_module |
| %mvn_package                                 | /usr/bin/python3 /usr/share/java-utils/mvn_package.py        |
| %{_sysconfdir}                               | /etc                                                         |
| %{buildroot}                                 | /root/rpmbuild/BUILDROOT/%{NAME}-%{VERSION}-%{RELEASE}.aarch64 |
| %{_jnidir}                                   | /usr/lib/java                                                |
| %{_includedir}                               | /usr/include                                                 |
| %{_datadir}                                  | /usr/share                                                   |
| %{_sharedstatedir}                           | /var/lib                                                     |
| %{_prefix}                                   | /usr                                                         |
| %{__ln_s}                                    | ln -s                                                        |
| %{_javadir}                                  | /usr/share/java                                              |
| ${httpfs.admin.port}                         | .admin.port                                                  |
| %{_sbindir}                                  | /usr/sbin                                                    |
| %{_sharedstatedir}                           | /var/lib                                                     |
| %systemd_preun                               | error: This macro requires some arguments                    |
| %{_libexecdir}                               | /usr/libexec                                                 |
| %{_unitdir}                                  | /usr/lib/systemd/system                                      |
| %{_libexecdir}                               | /usr/libexec                                                 |
| %mvn_package                                 | /usr/bin/python3 /usr/share/java-utils/mvn_package.py        |
| %{_sourcedir}                                | /root/rpmbuild/SOURCES                                       |
| %{_builddir}                                 | /root/rpmbuild/BUILD                                         |
| %{_buildrootdir}                             | /root/rpmbuild/BUILDROOT                                     |
| %{_rpmdir}                                   | /root/rpmbuild/RPMS                                          |
| %{_srcrpmdir}                                | /root/rpmbuild/SRPMS                                         |
| %{_var}                                      | /var                                                         |
| %mvn_file                                    | /usr/bin/python3 /usr/share/java-utils/mvn_file.py           |
| %{_mavenpomdir}                              | /usr/share/maven-poms                                        |
| %{_libexecdir}                               | /usr/libexec                                                 |



[%pom_add_dep](https://docs.fedoraproject.org/en-US/java-packaging-howto/manpage_pom_add_dep/)：add dependency to Maven POM file or Ivy module

```
%pom_add_dep groupId:artifactId[:version[:scope]] [POM-location]...[extra-XML]
```

[%pom_disable_module](https://docs.fedoraproject.org/en-US/java-packaging-howto/manpage_pom_disable_module/)：disable given project module in POM file

```
%pom_disable_module module-name [POM-location]...
```

[%mvn_package](https://docs.fedoraproject.org/en-US/java-packaging-howto/manpage_mvn_package/)：specify target package for Maven artifact(s)

```
%mvn_package artifact-coordinates [target-package]
```

[%mvn_file](https://docs.fedoraproject.org/en-US/java-packaging-howto/manpage_mvn_file/)：set installation location(s) for Maven artifact(s)

```
%mvn_file artifact-coordinates primary-file [secondary-file ...]
```



```shell
# Copy all jar files except those generated by the build
# $1 the src directory
# $2 the dest directory
copy_dep_jars()
{
  find $1 ! -name "hadoop-*.jar" -name "*.jar" | xargs install -m 0644 -t $2
  rm -f $2/tools-*.jar
}
```



`grep -v` ：显示不包含匹配文本的所有行。

```shell
# Create symlinks for jars from the build
# $1 the location to create the symlink
link_hadoop_jars()
{
  for f in `ls hadoop-* | grep -v tests | grep -v examples`
  do
    n=`echo $f | sed "s/-%{version}//"`
    if [ -L $1/$n ]
    then
      continue
    elif [ -e $1/$f ]
    then
      rm -f $1/$f $1/$n
    fi
    p=`find %{buildroot}%{_jnidir} %{buildroot}%{_javadir}/%{real_name} -name $n | sed "s#%{buildroot}##"`
    %{__ln_s} $p $1/$n
  done
}
```





# maven

groupId ：the unique identifier of the organization or group that created the project 

GroupID 是项目组织唯一的标识符，实际对应JAVA的包的结构，是main目录里java的目录结构。 



artifactId ：unique base name of the primary artifact being generated by this project 

ArtifactID是项目的唯一的标识符，实际对应项目的名称，就是项目根目录的名称。 



```shell
mvn install:install-file -DgroupId=com.google.protobuf -DartifactId=protoc -Dversion=2.5.0 -Dclassifier=linux-aarch_64 -Dpackaging=exe -Dfile=/usr/bin/protoc
```

install:maven中的install有`cp`的味道，所以，这句话是指：将`-Dfile=/usr/bin/protoc`的文件打包成`-Dpackaging=exe`，放到`/root/.m2/repository/`中`./com/google/protobuf/`文件夹中，并取名字为`-DartifactId -Dversion -Dclassifier`即`protoc-2.5.0-linux-aarch_64.exe `



# Q&A

[fedora - RPM build error: empty %files file debugfiles.list - Super User](https://superuser.com/questions/1091529/rpm-build-error-empty-files-file-debugfiles-list)