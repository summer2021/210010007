#!/usr/bin

cd $1
mkdir ../txt
for dir in `ls`
do
	cd $dir
	for files in `ls *.rpm`
	do
		rpm -ql $files > ${files}.txt
	done
	mv *.txt ../../txt
	cd ..
done