卷 文档 的文件夹 PATH 列表
卷序列号为 A010-4436
E:.
│  files.txt
│  
└─hadoop
    ├─client
    │      hadoop-client-api.jar
    │      hadoop-client-minicluster.jar
    │      hadoop-client-runtime.jar
    │      
    ├─common
    │  │  hadoop-common-tests.jar
    │  │  hadoop-common.jar
    │  │  hadoop-kms.jar
    │  │  hadoop-nfs.jar
    │  │  hadoop-registry-3.3.0.jar
    │  │  
    │  ├─jdiff
    │  │      Apache_Hadoop_Common_2.10.0.xml
    │  │      Apache_Hadoop_Common_2.6.0.xml
    │  │      Apache_Hadoop_Common_2.7.2.xml
    │  │      Apache_Hadoop_Common_2.8.0.xml
    │  │      Apache_Hadoop_Common_2.8.2.xml
    │  │      Apache_Hadoop_Common_2.8.3.xml
    │  │      Apache_Hadoop_Common_3.1.2.xml
    │  │      hadoop-core_0.20.0.xml
    │  │      hadoop-core_0.21.0.xml
    │  │      hadoop-core_0.22.0.xml
    │  │      hadoop_0.17.0.xml
    │  │      hadoop_0.18.1.xml
    │  │      hadoop_0.18.2.xml
    │  │      hadoop_0.18.3.xml
    │  │      hadoop_0.19.0.xml
    │  │      hadoop_0.19.1.xml
    │  │      hadoop_0.19.2.xml
    │  │      hadoop_0.20.0.xml
    │  │      hadoop_0.20.1.xml
    │  │      hadoop_0.20.2.xml
    │  │      Null.java
    │  │      
    │  ├─lib
    │  │      accessors-smart-1.2.jar
    │  │      animal-sniffer-annotations-1.17.jar
    │  │      asm-5.0.4.jar
    │  │      audience-annotations-0.5.0.jar
    │  │      avro-1.7.7.jar
    │  │      checker-qual-2.5.2.jar
    │  │      commons-beanutils-1.9.4.jar
    │  │      commons-cli-1.2.jar
    │  │      commons-codec-1.11.jar
    │  │      commons-collections-3.2.2.jar
    │  │      commons-compress-1.19.jar
    │  │      commons-configuration2-2.1.1.jar
    │  │      commons-daemon-1.0.13.jar
    │  │      commons-io-2.5.jar
    │  │      commons-lang3.jar
    │  │      commons-logging-1.1.3.jar
    │  │      commons-math3-3.1.1.jar
    │  │      commons-net-3.6.jar
    │  │      commons-text-1.4.jar
    │  │      curator-client.jar
    │  │      curator-framework.jar
    │  │      curator-recipes.jar
    │  │      dnsjava-2.1.7.jar
    │  │      failureaccess-1.0.jar
    │  │      gson-2.2.4.jar
    │  │      guava-27.0-jre.jar
    │  │      hadoop-annotations.jar
    │  │      hadoop-auth.jar
    │  │      hadoop-shaded-protobuf_3_7-1.0.0.jar
    │  │      htrace-core4-4.1.0-incubating.jar
    │  │      httpclient.jar
    │  │      httpcore.jar
    │  │      j2objc-annotations-1.1.jar
    │  │      jackson-annotations.jar
    │  │      jackson-core.jar
    │  │      jackson-core-asl-1.9.13.jar
    │  │      jackson-databind.jar
    │  │      jackson-jaxrs-1.9.13.jar
    │  │      jackson-mapper-asl-1.9.13.jar
    │  │      jackson-xc-1.9.13.jar
    │  │      javax.activation-api-1.2.0.jar
    │  │      javax.servlet-api-3.1.0.jar
    │  │      jaxb-api-2.2.11.jar
    │  │      jaxb-impl-2.2.3-1.jar
    │  │      jcip-annotations-1.0-1.jar
    │  │      jersey-core-1.19.jar
    │  │      jersey-json-1.19.jar
    │  │      jersey-server-1.19.jar
    │  │      jersey-servlet-1.19.jar
    │  │      jettison-1.1.jar
    │  │      jetty-http-9.4.20.v20190813.jar
    │  │      jetty-io-9.4.20.v20190813.jar
    │  │      jetty-security-9.4.20.v20190813.jar
    │  │      jetty-server-9.4.20.v20190813.jar
    │  │      jetty-servlet-9.4.20.v20190813.jar
    │  │      jetty-util-9.4.20.v20190813.jar
    │  │      jetty-webapp-9.4.20.v20190813.jar
    │  │      jetty-xml-9.4.20.v20190813.jar
    │  │      jsch-0.1.55.jar
    │  │      json-smart-2.3.jar
    │  │      jsp-api-2.1.jar
    │  │      jsr305-3.0.2.jar
    │  │      jsr311-api-1.1.1.jar
    │  │      jul-to-slf4j-1.7.25.jar
    │  │      kerb-admin-1.0.1.jar
    │  │      kerb-client-1.0.1.jar
    │  │      kerb-common-1.0.1.jar
    │  │      kerb-core-1.0.1.jar
    │  │      kerb-crypto-1.0.1.jar
    │  │      kerb-identity-1.0.1.jar
    │  │      kerb-server-1.0.1.jar
    │  │      kerb-simplekdc-1.0.1.jar
    │  │      kerb-util-1.0.1.jar
    │  │      kerby-asn1-1.0.1.jar
    │  │      kerby-config-1.0.1.jar
    │  │      kerby-pkix-1.0.1.jar
    │  │      kerby-util-1.0.1.jar
    │  │      kerby-xdr-1.0.1.jar
    │  │      listenablefuture-9999.0-empty-to-avoid-conflict-with-guava.jar
    │  │      log4j-1.2.17.jar
    │  │      metrics-core-3.2.4.jar
    │  │      netty-3.10.6.Final.jar
    │  │      nimbus-jose-jwt-7.9.jar
    │  │      paranamer-2.3.jar
    │  │      protobuf-java-2.5.0.jar
    │  │      re2j-1.1.jar
    │  │      slf4j-api-1.7.25.jar
    │  │      slf4j-log4j12-1.7.25.jar
    │  │      snappy-java-1.0.5.jar
    │  │      stax2-api-3.1.4.jar
    │  │      token-provider-1.0.1.jar
    │  │      woodstox-core-5.0.3.jar
    │  │      zookeeper.jar
    │  │      zookeeper-jute-3.5.6.jar
    │  │      
    │  ├─sources
    │  │      hadoop-common-sources.jar
    │  │      hadoop-common-test-sources.jar
    │  │      
    │  └─webapps
    │      └─static
    │              hadoop.css.gz
    │              
    ├─hdfs
    │  │  hadoop-hdfs-tests.jar
    │  │  hadoop-hdfs.jar
    │  │  hadoop-hdfs-client-tests.jar
    │  │  hadoop-hdfs-client.jar
    │  │  hadoop-hdfs-httpfs.jar
    │  │  hadoop-hdfs-native-client-tests.jar
    │  │  hadoop-hdfs-native-client.jar
    │  │  hadoop-hdfs-nfs.jar
    │  │  hadoop-hdfs-rbf-tests.jar
    │  │  hadoop-hdfs-rbf.jar
    │  │  
    │  ├─jdiff
    │  │      Apache_Hadoop_HDFS_2.10.0.xml
    │  │      Apache_Hadoop_HDFS_2.6.0.xml
    │  │      Apache_Hadoop_HDFS_2.7.2.xml
    │  │      Apache_Hadoop_HDFS_2.8.0.xml
    │  │      Apache_Hadoop_HDFS_2.8.2.xml
    │  │      Apache_Hadoop_HDFS_2.8.3.xml
    │  │      Apache_Hadoop_HDFS_2.9.1.xml
    │  │      Apache_Hadoop_HDFS_2.9.2.xml
    │  │      Apache_Hadoop_HDFS_3.0.0-alpha2.xml
    │  │      Apache_Hadoop_HDFS_3.0.0-alpha3.xml
    │  │      Apache_Hadoop_HDFS_3.0.0-alpha4.xml
    │  │      Apache_Hadoop_HDFS_3.0.0.xml
    │  │      Apache_Hadoop_HDFS_3.0.1.xml
    │  │      Apache_Hadoop_HDFS_3.0.2.xml
    │  │      Apache_Hadoop_HDFS_3.0.3.xml
    │  │      Apache_Hadoop_HDFS_3.1.0.xml
    │  │      Apache_Hadoop_HDFS_3.1.1.xml
    │  │      Apache_Hadoop_HDFS_3.1.2.xml
    │  │      Apache_Hadoop_HDFS_3.1.3.xml
    │  │      Apache_Hadoop_HDFS_3.2.0.xml
    │  │      Apache_Hadoop_HDFS_3.2.1.xml
    │  │      hadoop-hdfs_0.20.0.xml
    │  │      hadoop-hdfs_0.21.0.xml
    │  │      hadoop-hdfs_0.22.0.xml
    │  │      Null.java
    │  │      
    │  ├─lib
    │  │      accessors-smart-1.2.jar
    │  │      animal-sniffer-annotations-1.17.jar
    │  │      asm-5.0.4.jar
    │  │      audience-annotations-0.5.0.jar
    │  │      avro-1.7.7.jar
    │  │      checker-qual-2.5.2.jar
    │  │      commons-beanutils-1.9.4.jar
    │  │      commons-cli-1.2.jar
    │  │      commons-codec-1.11.jar
    │  │      commons-collections-3.2.2.jar
    │  │      commons-compress-1.19.jar
    │  │      commons-configuration2-2.1.1.jar
    │  │      commons-daemon-1.0.13.jar
    │  │      commons-io-2.5.jar
    │  │      commons-lang3-3.7.jar
    │  │      commons-logging-1.1.3.jar
    │  │      commons-math3-3.1.1.jar
    │  │      commons-net-3.6.jar
    │  │      commons-text-1.4.jar
    │  │      curator-client.jar
    │  │      curator-framework.jar
    │  │      curator-recipes.jar
    │  │      dnsjava-2.1.7.jar
    │  │      failureaccess-1.0.jar
    │  │      gson-2.2.4.jar
    │  │      guava-27.0-jre.jar
    │  │      hadoop-annotations.jar
    │  │      hadoop-auth.jar
    │  │      hadoop-shaded-protobuf_3_7-1.0.0.jar
    │  │      htrace-core4-4.1.0-incubating.jar
    │  │      httpclient.jar
    │  │      httpcore.jar
    │  │      j2objc-annotations-1.1.jar
    │  │      jackson-annotations.jar
    │  │      jackson-core.jar
    │  │      jackson-core-asl-1.9.13.jar
    │  │      jackson-databind.jar
    │  │      jackson-jaxrs-1.9.13.jar
    │  │      jackson-mapper-asl-1.9.13.jar
    │  │      jackson-xc-1.9.13.jar
    │  │      javax.activation-api-1.2.0.jar
    │  │      javax.servlet-api-3.1.0.jar
    │  │      jaxb-api-2.2.11.jar
    │  │      jaxb-impl-2.2.3-1.jar
    │  │      jcip-annotations-1.0-1.jar
    │  │      jersey-core-1.19.jar
    │  │      jersey-json-1.19.jar
    │  │      jersey-server-1.19.jar
    │  │      jersey-servlet-1.19.jar
    │  │      jettison-1.1.jar
    │  │      jetty-http-9.4.20.v20190813.jar
    │  │      jetty-io-9.4.20.v20190813.jar
    │  │      jetty-security-9.4.20.v20190813.jar
    │  │      jetty-server-9.4.20.v20190813.jar
    │  │      jetty-servlet-9.4.20.v20190813.jar
    │  │      jetty-util-9.4.20.v20190813.jar
    │  │      jetty-util-ajax-9.4.20.v20190813.jar
    │  │      jetty-webapp-9.4.20.v20190813.jar
    │  │      jetty-xml-9.4.20.v20190813.jar
    │  │      jsch-0.1.55.jar
    │  │      json-simple-1.1.1.jar
    │  │      json-smart-2.3.jar
    │  │      jsr305-3.0.2.jar
    │  │      jsr311-api-1.1.1.jar
    │  │      kerb-admin-1.0.1.jar
    │  │      kerb-client-1.0.1.jar
    │  │      kerb-common-1.0.1.jar
    │  │      kerb-core-1.0.1.jar
    │  │      kerb-crypto-1.0.1.jar
    │  │      kerb-identity-1.0.1.jar
    │  │      kerb-server-1.0.1.jar
    │  │      kerb-simplekdc-1.0.1.jar
    │  │      kerb-util-1.0.1.jar
    │  │      kerby-asn1-1.0.1.jar
    │  │      kerby-config-1.0.1.jar
    │  │      kerby-pkix-1.0.1.jar
    │  │      kerby-util-1.0.1.jar
    │  │      kerby-xdr-1.0.1.jar
    │  │      leveldbjni-all-1.8.jar
    │  │      listenablefuture-9999.0-empty-to-avoid-conflict-with-guava.jar
    │  │      log4j-1.2.17.jar
    │  │      netty-3.10.6.Final.jar
    │  │      netty-all.Final.jar
    │  │      nimbus-jose-jwt-7.9.jar
    │  │      okhttp-2.7.5.jar
    │  │      okio-1.6.0.jar
    │  │      paranamer-2.3.jar
    │  │      protobuf-java-2.5.0.jar
    │  │      re2j-1.1.jar
    │  │      snappy-java-1.0.5.jar
    │  │      stax2-api-3.1.4.jar
    │  │      token-provider-1.0.1.jar
    │  │      woodstox-core-5.0.3.jar
    │  │      zookeeper.jar
    │  │      zookeeper-jute-3.5.6.jar
    │  │      
    │  ├─sources
    │  │      hadoop-hdfs-sources.jar
    │  │      hadoop-hdfs-test-sources.jar
    │  │      hadoop-hdfs-client-sources.jar
    │  │      hadoop-hdfs-client-test-sources.jar
    │  │      hadoop-hdfs-native-client-sources.jar
    │  │      hadoop-hdfs-native-client-test-sources.jar
    │  │      hadoop-hdfs-rbf-sources.jar
    │  │      hadoop-hdfs-rbf-test-sources.jar
    │  │      
    │  └─webapps
    │      ├─datanode
    │      │  │  datanode.html
    │      │  │  dn.js
    │      │  │  index.html
    │      │  │  robots.txt
    │      │  │  
    │      │  └─WEB-INF
    │      │          web.xml
    │      │          
    │      ├─hdfs
    │      │  │  dfshealth.html
    │      │  │  dfshealth.js
    │      │  │  explorer.html
    │      │  │  explorer.js
    │      │  │  index.html
    │      │  │  robots.txt
    │      │  │  
    │      │  └─WEB-INF
    │      │          web.xml
    │      │          
    │      ├─journal
    │      │  │  index.html
    │      │  │  robots.txt
    │      │  │  
    │      │  └─WEB-INF
    │      │          web.xml
    │      │          
    │      ├─nfs3
    │      │  └─WEB-INF
    │      │          web.xml
    │      │          
    │      ├─router
    │      │  │  explorer.html
    │      │  │  explorer.js
    │      │  │  federationhealth.html
    │      │  │  federationhealth.js
    │      │  │  index.html
    │      │  │  robots.txt
    │      │  │  
    │      │  └─WEB-INF
    │      │          web.xml
    │      │          
    │      ├─secondary
    │      │  │  index.html
    │      │  │  robots.txt
    │      │  │  snn.js
    │      │  │  status.html
    │      │  │  
    │      │  └─WEB-INF
    │      │          web.xml
    │      │          
    │      └─static
    │          │  d3-v4.1.1.min.js
    │          │  d3-v4.1.1.min.js.gz
    │          │  dataTables.bootstrap.css
    │          │  dataTables.bootstrap.css.gz
    │          │  dataTables.bootstrap.js
    │          │  dataTables.bootstrap.js.gz
    │          │  dfs-dust.js
    │          │  dfs-dust.js.gz
    │          │  dust-full-2.0.0.min.js
    │          │  dust-full-2.0.0.min.js.gz
    │          │  dust-helpers-1.1.1.min.js
    │          │  dust-helpers-1.1.1.min.js.gz
    │          │  hadoop.css
    │          │  hadoop.css.gz
    │          │  jquery-3.4.1.min.js
    │          │  jquery-3.4.1.min.js.gz
    │          │  jquery.dataTables.min.js
    │          │  jquery.dataTables.min.js.gz
    │          │  json-bignum.js
    │          │  json-bignum.js.gz
    │          │  moment.min.js
    │          │  moment.min.js.gz
    │          │  rbf.css
    │          │  rest-csrf.js
    │          │  rest-csrf.js.gz
    │          │  
    │          └─bootstrap-3.4.1
    │              ├─css
    │              │      bootstrap-editable.css
    │              │      bootstrap-editable.css.gz
    │              │      bootstrap-theme.css
    │              │      bootstrap-theme.css.gz
    │              │      bootstrap-theme.css.map
    │              │      bootstrap-theme.min.css
    │              │      bootstrap-theme.min.css.gz
    │              │      bootstrap-theme.min.css.map
    │              │      bootstrap.css
    │              │      bootstrap.css.gz
    │              │      bootstrap.css.map
    │              │      bootstrap.min.css
    │              │      bootstrap.min.css.gz
    │              │      bootstrap.min.css.map
    │              │      
    │              ├─fonts
    │              │      glyphicons-halflings-regular.eot
    │              │      glyphicons-halflings-regular.svg
    │              │      glyphicons-halflings-regular.ttf
    │              │      glyphicons-halflings-regular.woff
    │              │      glyphicons-halflings-regular.woff2
    │              │      
    │              └─js
    │                      bootstrap-editable.min.js
    │                      bootstrap-editable.min.js.gz
    │                      bootstrap.js
    │                      bootstrap.js.gz
    │                      bootstrap.min.js
    │                      bootstrap.min.js.gz
    │                      npm.js
    │                      npm.js.gz
    │                      
    ├─mapreduce
    │  │  hadoop-mapreduce-client-app-3.3.0.jar
    │  │  hadoop-mapreduce-client-common-3.3.0.jar
    │  │  hadoop-mapreduce-client-core-3.3.0.jar
    │  │  hadoop-mapreduce-client-hs-3.3.0.jar
    │  │  hadoop-mapreduce-client-hs-plugins-3.3.0.jar
    │  │  hadoop-mapreduce-client-jobclient-3.3.0-tests.jar
    │  │  hadoop-mapreduce-client-jobclient-3.3.0.jar
    │  │  hadoop-mapreduce-client-nativetask-3.3.0.jar
    │  │  hadoop-mapreduce-client-shuffle-3.3.0.jar
    │  │  hadoop-mapreduce-client-uploader-3.3.0.jar
    │  │  hadoop-mapreduce-examples-3.3.0.jar
    │  │  
    │  ├─jdiff
    │  │      Apache_Hadoop_MapReduce_Common_2.10.0.xml
    │  │      Apache_Hadoop_MapReduce_Common_2.6.0.xml
    │  │      Apache_Hadoop_MapReduce_Common_2.7.2.xml
    │  │      Apache_Hadoop_MapReduce_Common_2.8.0.xml
    │  │      Apache_Hadoop_MapReduce_Common_2.8.2.xml
    │  │      Apache_Hadoop_MapReduce_Common_2.8.3.xml
    │  │      Apache_Hadoop_MapReduce_Common_3.1.0.xml
    │  │      Apache_Hadoop_MapReduce_Core_2.10.0.xml
    │  │      Apache_Hadoop_MapReduce_Core_2.6.0.xml
    │  │      Apache_Hadoop_MapReduce_Core_2.7.2.xml
    │  │      Apache_Hadoop_MapReduce_Core_2.8.0.xml
    │  │      Apache_Hadoop_MapReduce_Core_2.8.2.xml
    │  │      Apache_Hadoop_MapReduce_Core_2.8.3.xml
    │  │      Apache_Hadoop_MapReduce_Core_3.1.0.xml
    │  │      Apache_Hadoop_MapReduce_Core_3.1.2.xml
    │  │      Apache_Hadoop_MapReduce_JobClient_2.10.0.xml
    │  │      Apache_Hadoop_MapReduce_JobClient_2.6.0.xml
    │  │      Apache_Hadoop_MapReduce_JobClient_2.7.2.xml
    │  │      Apache_Hadoop_MapReduce_JobClient_2.8.0.xml
    │  │      Apache_Hadoop_MapReduce_JobClient_2.8.2.xml
    │  │      Apache_Hadoop_MapReduce_JobClient_2.8.3.xml
    │  │      Apache_Hadoop_MapReduce_JobClient_3.1.0.xml
    │  │      Apache_Hadoop_MapReduce_JobClient_3.1.2.xml
    │  │      Null.java
    │  │      
    │  ├─lib-examples
    │  │      hsqldb-2.3.4.jar
    │  │      
    │  └─sources
    │          hadoop-mapreduce-client-app-sources.jar
    │          hadoop-mapreduce-client-app-test-sources.jar
    │          hadoop-mapreduce-client-common-sources.jar
    │          hadoop-mapreduce-client-common-test-sources.jar
    │          hadoop-mapreduce-client-core-sources.jar
    │          hadoop-mapreduce-client-core-test-sources.jar
    │          hadoop-mapreduce-client-hs-sources.jar
    │          hadoop-mapreduce-client-hs-test-sources.jar
    │          hadoop-mapreduce-client-hs-plugins-sources.jar
    │          hadoop-mapreduce-client-hs-plugins-test-sources.jar
    │          hadoop-mapreduce-client-jobclient-sources.jar
    │          hadoop-mapreduce-client-jobclient-test-sources.jar
    │          hadoop-mapreduce-client-nativetask-sources.jar
    │          hadoop-mapreduce-client-nativetask-test-sources.jar
    │          hadoop-mapreduce-client-shuffle-sources.jar
    │          hadoop-mapreduce-client-shuffle-test-sources.jar
    │          hadoop-mapreduce-examples-sources.jar
    │          hadoop-mapreduce-examples-test-sources.jar
    │          
    ├─tools
    │  ├─dynamometer
    │  │  ├─dynamometer-blockgen
    │  │  │  └─bin
    │  │  │          generate-block-lists.sh
    │  │  │          
    │  │  ├─dynamometer-infra
    │  │  │  └─bin
    │  │  │          create-slim-hadoop-tar.sh
    │  │  │          parse-metrics.sh
    │  │  │          start-dynamometer-cluster.sh
    │  │  │          upload-fsimage.sh
    │  │  │          
    │  │  └─dynamometer-workload
    │  │      └─bin
    │  │              parse-start-timestamp.sh
    │  │              start-workload.sh
    │  │              
    │  ├─lib
    │  │      aliyun-java-sdk-core-3.4.0.jar
    │  │      aliyun-java-sdk-ecs-4.2.0.jar
    │  │      aliyun-java-sdk-ram-3.0.0.jar
    │  │      aliyun-java-sdk-sts-3.0.0.jar
    │  │      aliyun-sdk-oss-3.4.1.jar
    │  │      aws-java-sdk-bundle.jar
    │  │      azure-data-lake-store-sdk.jar
    │  │      azure-keyvault-core-1.0.0.jar
    │  │      azure-storage-7.0.0.jar
    │  │      hadoop-aliyun.jar
    │  │      hadoop-archive-logs.jar
    │  │      hadoop-archives.jar
    │  │      hadoop-aws.jar
    │  │      hadoop-azure.jar
    │  │      hadoop-azure-datalake.jar
    │  │      hadoop-client.jar
    │  │      hadoop-datajoin.jar
    │  │      hadoop-distcp.jar
    │  │      hadoop-dynamometer-blockgen.jar
    │  │      hadoop-dynamometer-infra.jar
    │  │      hadoop-dynamometer-workload.jar
    │  │      hadoop-extras.jar
    │  │      hadoop-fs2img.jar
    │  │      hadoop-gridmix.jar
    │  │      hadoop-kafka.jar
    │  │      hadoop-minicluster.jar
    │  │      hadoop-openstack.jar
    │  │      hadoop-resourceestimator.jar
    │  │      hadoop-rumen.jar
    │  │      hadoop-sls.jar
    │  │      hadoop-streaming.jar
    │  │      hamcrest-core.jar
    │  │      jdk.tools-1.8.jar
    │  │      jdom-1.1.jar
    │  │      junit-4.12.jar
    │  │      kafka-clients-2.4.0.jar
    │  │      lz4-java-1.6.0.jar
    │  │      ojalgo-43.0.jar
    │  │      wildfly-openssl-1.0.7.Final.jar
    │  │      zstd-jni-1.4.3-1.jar
    │  │      
    │  ├─resourceestimator
    │  │  ├─bin
    │  │  │      estimator.cmd
    │  │  │      estimator.sh
    │  │  │      start-estimator.cmd
    │  │  │      start-estimator.sh
    │  │  │      stop-estimator.cmd
    │  │  │      stop-estimator.sh
    │  │  │      
    │  │  ├─conf
    │  │  │      resourceestimator-config.xml
    │  │  │      
    │  │  └─data
    │  │          resourceEstimatorService.txt
    │  │          
    │  ├─sls
    │  │  ├─bin
    │  │  │      rumen2sls.sh
    │  │  │      slsrun.sh
    │  │  │      
    │  │  ├─html
    │  │  │  │  showSimulationTrace.html
    │  │  │  │  simulate.html.template
    │  │  │  │  simulate.info.html.template
    │  │  │  │  track.html.template
    │  │  │  │  
    │  │  │  ├─css
    │  │  │  │      bootstrap-responsive.min.css
    │  │  │  │      bootstrap.min.css
    │  │  │  │      
    │  │  │  └─js
    │  │  │      └─thirdparty
    │  │  │              bootstrap.min.js
    │  │  │              d3.v3.js
    │  │  │              jquery.js
    │  │  │              
    │  │  ├─sample-conf
    │  │  │      capacity-scheduler.xml
    │  │  │      fair-scheduler.xml
    │  │  │      log4j.properties
    │  │  │      sls-runner.xml
    │  │  │      yarn-site.xml
    │  │  │      
    │  │  └─sample-data
    │  │          2jobs2min-rumen-jh.json
    │  │          
    │  └─sources
    │          hadoop-archive-logs-sources.jar
    │          hadoop-archive-logs--test-sources.jar
    │          hadoop-archives-sources.jar
    │          hadoop-archives-test-sources.jar
    │          hadoop-datajoin-sources.jar
    │          hadoop-datajoin-test-sources.jar
    │          hadoop-distcp-sources.jar
    │          hadoop-distcp-test-sources.jar
    │          hadoop-dynamometer-blockgen-3.3.0-sources.jar
    │          hadoop-dynamometer-blockgen-3.3.0-test-sources.jar
    │          hadoop-dynamometer-infra-3.3.0-sources.jar
    │          hadoop-dynamometer-infra-3.3.0-test-sources.jar
    │          hadoop-dynamometer-workload-3.3.0-sources.jar
    │          hadoop-dynamometer-workload-3.3.0-test-sources.jar
    │          hadoop-extras-3.3.0-sources.jar
    │          hadoop-extras-3.3.0-test-sources.jar
    │          hadoop-gridmix-3.3.0-sources.jar
    │          hadoop-gridmix-3.3.0-test-sources.jar
    │          hadoop-resourceestimator-3.3.0-sources.jar
    │          hadoop-resourceestimator-3.3.0-test-sources.jar
    │          hadoop-rumen-3.3.0-sources.jar
    │          hadoop-rumen-3.3.0-test-sources.jar
    │          hadoop-sls-3.3.0-sources.jar
    │          hadoop-sls-3.3.0-test-sources.jar
    │          hadoop-streaming-3.3.0-sources.jar
    │          hadoop-streaming-3.3.0-test-sources.jar
    │          
    └─yarn
        │  hadoop-yarn-api-3.3.0.jar
        │  hadoop-yarn-applications-catalog-webapp-3.3.0.war
        │  hadoop-yarn-applications-distributedshell-3.3.0.jar
        │  hadoop-yarn-applications-mawo-core-3.3.0.jar
        │  hadoop-yarn-applications-unmanaged-am-launcher-3.3.0.jar
        │  hadoop-yarn-client-3.3.0.jar
        │  hadoop-yarn-common-3.3.0.jar
        │  hadoop-yarn-registry-3.3.0.jar
        │  hadoop-yarn-server-applicationhistoryservice-3.3.0.jar
        │  hadoop-yarn-server-common-3.3.0.jar
        │  hadoop-yarn-server-nodemanager-3.3.0.jar
        │  hadoop-yarn-server-resourcemanager-3.3.0.jar
        │  hadoop-yarn-server-router-3.3.0.jar
        │  hadoop-yarn-server-sharedcachemanager-3.3.0.jar
        │  hadoop-yarn-server-tests-3.3.0.jar
        │  hadoop-yarn-server-timeline-pluginstorage-3.3.0.jar
        │  hadoop-yarn-server-web-proxy-3.3.0.jar
        │  hadoop-yarn-services-api-3.3.0.jar
        │  hadoop-yarn-services-core-3.3.0.jar
        │  
        ├─csi
        │  │  hadoop-yarn-csi-3.3.0.jar
        │  │  
        │  └─lib
        │          annotations-4.1.1.4.jar
        │          error_prone_annotations-2.3.3.jar
        │          grpc-api-1.26.0.jar
        │          grpc-context-1.26.0.jar
        │          grpc-core-1.26.0.jar
        │          grpc-netty-1.26.0.jar
        │          grpc-protobuf-1.26.0.jar
        │          grpc-protobuf-lite-1.26.0.jar
        │          grpc-stub-1.26.0.jar
        │          guava-20.0.jar
        │          javax.annotation-api-1.3.2.jar
        │          netty-buffer-4.1.42.Final.jar
        │          netty-codec-4.1.42.Final.jar
        │          netty-codec-http-4.1.42.Final.jar
        │          netty-codec-http2-4.1.42.Final.jar
        │          netty-codec-socks-4.1.42.Final.jar
        │          netty-common-4.1.42.Final.jar
        │          netty-handler-4.1.42.Final.jar
        │          netty-handler-proxy-4.1.42.Final.jar
        │          netty-resolver-4.1.42.Final.jar
        │          netty-transport-4.1.42.Final.jar
        │          opencensus-api-0.24.0.jar
        │          opencensus-contrib-grpc-metrics-0.24.0.jar
        │          perfmark-api-0.19.0.jar
        │          proto-google-common-protos-1.12.0.jar
        │          protobuf-java-3.6.1.jar
        │          
        ├─lib
        │      aopalliance-1.0.jar
        │      asm-analysis-7.1.jar
        │      asm-commons-7.1.jar
        │      asm-tree-7.1.jar
        │      bcpkix-jdk15on-1.60.jar
        │      bcprov-jdk15on-1.60.jar
        │      ehcache-3.3.1.jar
        │      fst-2.50.jar
        │      geronimo-jcache_1.0_spec-1.0-alpha-1.jar
        │      guice-4.0.jar
        │      guice-servlet-4.0.jar
        │      HikariCP-java7-2.4.12.jar
        │      jackson-jaxrs-base-2.10.3.jar
        │      jackson-jaxrs-json-provider-2.10.3.jar
        │      jackson-module-jaxb-annotations-2.10.3.jar
        │      jakarta.activation-api-1.2.1.jar
        │      jakarta.xml.bind-api-2.3.2.jar
        │      java-util-1.9.0.jar
        │      javax-websocket-client-impl-9.4.20.v20190813.jar
        │      javax-websocket-server-impl-9.4.20.v20190813.jar
        │      javax.inject-1.jar
        │      javax.websocket-api-1.0.jar
        │      javax.websocket-client-api-1.0.jar
        │      jersey-client-1.19.jar
        │      jersey-guice-1.19.jar
        │      jetty-annotations-9.4.20.v20190813.jar
        │      jetty-client-9.4.20.v20190813.jar
        │      jetty-jndi-9.4.20.v20190813.jar
        │      jetty-plus-9.4.20.v20190813.jar
        │      jline-3.9.0.jar
        │      jna-5.2.0.jar
        │      json-io-2.5.1.jar
        │      metrics-core-3.2.4.jar
        │      mssql-jdbc-6.2.1.jre7.jar
        │      objenesis-2.6.jar
        │      snakeyaml-1.16.jar
        │      swagger-annotations-1.5.4.jar
        │      websocket-api-9.4.20.v20190813.jar
        │      websocket-client-9.4.20.v20190813.jar
        │      websocket-common-9.4.20.v20190813.jar
        │      websocket-server-9.4.20.v20190813.jar
        │      websocket-servlet-9.4.20.v20190813.jar
        │      
        ├─sources
        │      hadoop-yarn-api-3.3.0-sources.jar
        │      hadoop-yarn-api-3.3.0-test-sources.jar
        │      hadoop-yarn-applications-distributedshell-3.3.0-sources.jar
        │      hadoop-yarn-applications-distributedshell-3.3.0-test-sources.jar
        │      hadoop-yarn-applications-unmanaged-am-launcher-3.3.0-sources.jar
        │      hadoop-yarn-applications-unmanaged-am-launcher-3.3.0-test-sources.jar
        │      hadoop-yarn-client-3.3.0-sources.jar
        │      hadoop-yarn-client-3.3.0-test-sources.jar
        │      hadoop-yarn-common-3.3.0-sources.jar
        │      hadoop-yarn-common-3.3.0-test-sources.jar
        │      hadoop-yarn-server-applicationhistoryservice-3.3.0-sources.jar
        │      hadoop-yarn-server-applicationhistoryservice-3.3.0-test-sources.jar
        │      hadoop-yarn-server-common-3.3.0-sources.jar
        │      hadoop-yarn-server-common-3.3.0-test-sources.jar
        │      hadoop-yarn-server-nodemanager-3.3.0-sources.jar
        │      hadoop-yarn-server-nodemanager-3.3.0-test-sources.jar
        │      hadoop-yarn-server-resourcemanager-3.3.0-sources.jar
        │      hadoop-yarn-server-resourcemanager-3.3.0-test-sources.jar
        │      hadoop-yarn-server-tests-3.3.0-sources.jar
        │      hadoop-yarn-server-tests-3.3.0-test-sources.jar
        │      hadoop-yarn-server-web-proxy-3.3.0-sources.jar
        │      hadoop-yarn-server-web-proxy-3.3.0-test-sources.jar
        │      hadoop-yarn-services-api-3.3.0-sources.jar
        │      hadoop-yarn-services-api-3.3.0-test-sources.jar
        │      hadoop-yarn-services-core-3.3.0-sources.jar
        │      hadoop-yarn-services-core-3.3.0-test-sources.jar
        │      
        ├─test
        │      hadoop-yarn-server-tests-3.3.0-tests.jar
        │      
        ├─timelineservice
        │  │  hadoop-yarn-server-timelineservice-3.3.0.jar
        │  │  hadoop-yarn-server-timelineservice-documentstore-3.3.0.jar
        │  │  hadoop-yarn-server-timelineservice-hbase-client-3.3.0.jar
        │  │  hadoop-yarn-server-timelineservice-hbase-common-3.3.0.jar
        │  │  hadoop-yarn-server-timelineservice-hbase-coprocessor-3.3.0.jar
        │  │  
        │  ├─lib
        │  │      azure-cosmosdb-2.4.5.jar
        │  │      azure-cosmosdb-commons-2.4.5.jar
        │  │      azure-cosmosdb-direct-2.4.5.jar
        │  │      azure-cosmosdb-gateway-2.4.5.jar
        │  │      commons-collections4-4.2.jar
        │  │      commons-csv-1.0.jar
        │  │      commons-digester-1.8.1.jar
        │  │      commons-lang-2.6.jar
        │  │      commons-validator-1.6.jar
        │  │      hbase-annotations-1.4.8.jar
        │  │      hbase-client-1.4.8.jar
        │  │      hbase-common-1.4.8.jar
        │  │      hbase-protocol-1.4.8.jar
        │  │      htrace-core-3.1.0-incubating.jar
        │  │      java-uuid-generator-3.1.4.jar
        │  │      jcodings-1.0.13.jar
        │  │      joni-2.1.2.jar
        │  │      metrics-core-2.2.0.jar
        │  │      rxjava-1.3.8.jar
        │  │      rxjava-extras-0.8.0.17.jar
        │  │      rxjava-string-1.1.1.jar
        │  │      rxnetty-0.4.20.jar
        │  │      
        │  └─test
        │          hadoop-yarn-server-timelineservice-hbase-tests-3.3.0.jar
        │          
        └─yarn-service-examples
            ├─appcatalog
            │      appcatalog.json
            │      
            ├─httpd
            │      httpd-proxy.conf
            │      httpd.json
            │      
            ├─httpd-no-dns
            │      httpd-no-dns.json
            │      httpd-proxy-no-dns.conf
            │      
            └─sleeper
                    sleeper.json
                    
